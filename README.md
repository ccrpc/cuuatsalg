# CCRPC Processing Algorithms
QGIS 3 processing algorithms for the Champaign County Regional Planning
Commission

## Algorithms
### Attachments
- Export ArcGIS attachments
### Geocoding
- Geocode Addresses Using Nominatim
### Project management
- Create project folder
### Street network
- Copy network attributes
- Create network match table

## Running Tests
```
python -m unittest discover
```

## Credits
CCRPC Processing Algorithms was developed by Matt Yoder and Edmond Lai for the
[Champaign County Region Planning Commission][1].

## License
CCRPC Processing Algorithms is available under the terms of the [BSD 3-clause
license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/ccrpc-qgis-algorithms/blob/master/LICENSE.md
