from .plugin import CCRPCAlgorithmsPlugin


def classFactory(iface):
    return CCRPCAlgorithmsPlugin()
