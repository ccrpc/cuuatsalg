from qgis.core import QgsApplication
from .provider import CCRPCAlgorithmProvider


class CCRPCAlgorithmsPlugin:

    def __init__(self):
        self.provider = CCRPCAlgorithmProvider()

    def initGui(self):
        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        QgsApplication.processingRegistry().removeProvider(self.provider)
